package com.testcase;

import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.http.HttpRequest;
import com.http.ResponseResult;
import com.utility.JsonUtility;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class GetCartInfo_Test {
	
	public static String Domain = null;

	@BeforeSuite
	public void beforeSuite() {
	}

	@AfterSuite
	public void afterSuite() {
	}

	@BeforeTest
	public void beforeTest() {
	}

	@AfterTest
	public void afterTest() {
	}

	@BeforeClass
	public void beforeClass() {
	}

	@AfterClass
	public void afterClass() {
	}

	@Parameters({ "environment" })
	@BeforeMethod
	public void beforeMethod(String environment) {
		Domain = environment;
	}

	@AfterMethod
	public void afterMethod() {
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Get_GetCartInfo(Map<?, ?> param) throws Exception {
		ResponseResult actual = HttpRequest.httpGet(((String) param.get("URL")).replace("{0}", Domain), (String) param.get("params"), (String) param.get("headers"));
		ProcessingReport report = JsonUtility.checkJsonSchema((String) param.get("schema"), actual.getResponse());
		
		Assert.assertEquals(actual.getStatus(), 200);		
		Assert.assertTrue(report.isSuccess(), report.toString());
	}

}
