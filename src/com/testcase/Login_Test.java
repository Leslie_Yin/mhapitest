package com.testcase;

import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.http.HttpRequest;
import com.http.ResponseResult;
import com.utility.JsonUtility;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Login_Test {
	
	public static String Domain = null;

	@BeforeSuite
	public void beforeSuite() {
	}

	@AfterSuite
	public void afterSuite() {
	}

	@BeforeTest
	public void beforeTest() {
	}

	@AfterTest
	public void afterTest() {
	}

	@BeforeClass
	public void beforeClass() {
	}

	@AfterClass
	public void afterClass() {
	}

	@Parameters({ "environment" })
	@BeforeMethod
	public void beforeMethod(String environment) {
		Domain = environment;
	}

	@AfterMethod
	public void afterMethod() {
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Post_Login_WithCorrectEmailaddress_WithCorrectPassword(Map<?, ?> param) throws Exception {
		ResponseResult actual = HttpRequest.httpPost(((String) param.get("URL")).replace("{0}", Domain), (String) param.get("headers"), (String) param.get("request"));
		ProcessingReport report = JsonUtility.checkJsonSchema((String) param.get("schema"), actual.getResponse());
		
		Assert.assertEquals(actual.getStatus(), 200);		
		Assert.assertTrue(report.isSuccess(), report.toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Post_Login_WithIncorrectEmailaddress_WithCorrectPassword(Map<?, ?> param) throws Exception {
		ResponseResult actual = HttpRequest.httpPost(((String) param.get("URL")).replace("{0}", Domain), (String) param.get("headers"), (String) param.get("request"));

		Assert.assertEquals(actual.getStatus(), 403);
		Assert.assertEquals(JsonUtility.toObject(actual.getResponse()), JsonUtility.toObject((String) param.get("response")));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Post_Login_WithCorrectEmailaddress_WithIncorrectPassword(Map<?, ?> param) throws Exception {
		ResponseResult actual = HttpRequest.httpPost(((String) param.get("URL")).replace("{0}", Domain), (String) param.get("headers"), (String) param.get("request"));

		Assert.assertEquals(actual.getStatus(), 403);
		Assert.assertEquals(JsonUtility.toObject(actual.getResponse()), JsonUtility.toObject((String) param.get("response")));
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Post_Login_WithIncorrectEmailaddress_WithIncorrectPassword(Map<?, ?> param) throws Exception {
		ResponseResult actual = HttpRequest.httpPost(((String) param.get("URL")).replace("{0}", Domain), (String) param.get("headers"), (String) param.get("request"));

		Assert.assertEquals(actual.getStatus(), 403);
		Assert.assertEquals(JsonUtility.toObject(actual.getResponse()), JsonUtility.toObject((String) param.get("response")));
	}
}
