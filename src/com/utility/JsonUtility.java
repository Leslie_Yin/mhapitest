package com.utility;

import com.google.gson.Gson;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

public class JsonUtility {
	public static Object toObject(String Json) {
		Object result = null;
		try {
			Gson gson = new Gson();
			result=gson.fromJson(Json, Object.class);
		} catch (Exception e) {
			System.out.println("Json can't deserialization. " + Json);
			e.printStackTrace();
		}
		return result;
	}
	
	public static List<?> toList(String Json) {
		List<?> result = null;
		try {
			Gson gson = new Gson();
			result=gson.fromJson(Json, List.class);
		} catch (Exception e) {
			System.out.println("Json can't deserialization. " + Json);
			e.printStackTrace();
		}
		return result;
	}
	
	public static ProcessingReport checkJsonSchema(String jsonSchema, String json)
	{
		ProcessingReport processingReport = null;
		try {
			JsonNode schema = JsonLoader.fromString(jsonSchema);
			JsonNode data = JsonLoader.fromString(json);
			processingReport = JsonSchemaFactory.byDefault().getValidator().validateUnchecked(schema, data);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return processingReport;
	}
}
