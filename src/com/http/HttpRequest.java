package com.http;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class HttpRequest {

	public static ResponseResult httpGet(String url, String param, String headers) throws Exception {
		ResponseResult result = new ResponseResult();
		CloseableHttpClient httpclient = HttpClients.createDefault();
		//HttpHost proxy = new HttpHost("10.148.51.10", 8080);
		//RequestConfig config = RequestConfig.custom().setProxy(proxy).build();

		HttpGet httpget = (null == param || param.isEmpty()) ? new HttpGet(url) : new HttpGet(url + "?" + param);
		//httpget.setConfig(config);
		if (null != headers && !headers.isEmpty()) {
			for (String header : headers.split("/r/n")) {
				if (null != header && header.contains(":")) {
					httpget.addHeader(header.split(":")[0], header.substring(header.indexOf(":") + 1));
				}
			}
		}

		HttpResponse response = httpclient.execute(httpget);
		result.setStatus(response.getStatusLine().getStatusCode());
		BufferedReader br = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent(), Charset.forName("UTF-8")));
		String resp = "";
		String line;
		while ((line = br.readLine()) != null) {
			resp += line;
		}
		br.close();
		result.setResponse(resp);
		return result;
	}

	public static ResponseResult httpPost(String url, String headers, String request) throws Exception {
		ResponseResult result = new ResponseResult();
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpHost proxy = new HttpHost("10.148.51.10", 8080);
		RequestConfig config = RequestConfig.custom().setProxy(proxy).build();

		HttpPost httppost = new HttpPost(url);
		httppost.setConfig(config);
		if (null != headers && !headers.isEmpty()) {
			for (String header : headers.split("/r/n")) {
				if (null != header && header.contains(":")) {
					httppost.addHeader(header.split(":")[0], header.substring(header.indexOf(":") + 1));
				}
			}
		}
		StringEntity se = new StringEntity(request, "UTF-8");
		httppost.setEntity(se);

		HttpResponse response = httpclient.execute(httppost);
		result.setStatus(response.getStatusLine().getStatusCode());
		BufferedReader br = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent(), Charset.forName("UTF-8")));
		String resp = "";
		String line;
		while ((line = br.readLine()) != null) {
			resp += line;
		}
		br.close();
		result.setResponse(resp);
		return result;
	}
	
	public static Map<String, String> getEachParam(String params)
	{
		Map<String, String> result = new HashMap<String, String>();
		if(null != params)
		{
			for(String keyValue : params.split("&"))
			{
				if(null != keyValue && keyValue.contains("="))
				{
					result.put(keyValue.split("=")[0], keyValue.split("=")[1]);
				}
			}
		}
		return result;
	}
}
